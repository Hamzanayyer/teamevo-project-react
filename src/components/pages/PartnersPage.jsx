import React from "react";
import HeaderPartner from "./../components/HeaderPartner/HeaderPartner";
import PartnersList from "./../containers/partners/PartnersList";
const PartnersPage = () => {
  return (
    <>
      <HeaderPartner />
      <PartnersList />
    </>
  );
};
export default PartnersPage;

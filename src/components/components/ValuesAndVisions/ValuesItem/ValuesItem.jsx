import React from "react";
import { Row, Col } from "react-bootstrap";
import "./ValuesItem.css";

const ValuesItem = ({ data }) => {
  return (
    <Row
      className='justify-content-md-center values-item pt-4 pb-4'
      id={data.filterId}>
      <Col md={12} lg={8}>
        <h2>{data.title}</h2>
        <p>{data.text}</p>
        {data.link ? (
          <div className='d-flex justify-content-center pt-4 pb-2'>
            <a href={data.url} target='_blank' rel='noopener noreferrer'>
              {data.link}
            </a>
          </div>
        ) : (
          <></>
        )}
      </Col>
    </Row>
  );
};

export default ValuesItem;
